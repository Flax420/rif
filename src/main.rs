extern crate gif;
extern crate pancurses;
extern crate raster;

use gif::{Decoder, Reader, SetParameter};
use pancurses::{endwin, initscr, noecho, Input};
use std::env;
use std::fs::File;
use std::prelude::*;
use std::thread::sleep;
use std::time::Duration;

fn asciify(in_image: &raster::Image) -> String {
    let ascii_palette: String =
        "$@B8$WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/()1{}[]?-_+~<>i!lI;:,\"^`'.  ".to_string();
    let char_vec: Vec<char> = ascii_palette.chars().collect();
    let mut result_string: String = "".to_string();
    for _y in 0..in_image.height {
        for _x in 0..in_image.width {
            let char_to_add =
                (in_image.get_pixel(_x, _y).unwrap().r as f32 / 255.0) * char_vec.len() as f32;
            result_string += &char_vec[char_to_add as usize].to_string();
        }
        result_string += "\n";
    }
    return result_string;
}

fn main() -> std::io::Result<()> {
    // Initializing decoding
    let args: Vec<_> = env::args().collect();
    if args.len() == 1 {
        println!("Error: No gif supplied!");
        return Ok(());
    }
    if !args[1].ends_with(".gif") {
        println!("Error: Incorrect file format!");
        return Ok(());
    }
    let file_in = File::open(args[1].to_string())?;
    let mut decoder = Decoder::new(file_in);
    decoder.set(gif::ColorOutput::RGBA);

    let mut decoder = decoder.read_info().unwrap();
    let mut prev_raster = raster::Image::blank(decoder.width() as i32, decoder.height() as i32);
    let mut string_vector: Vec<String> = Vec::new();

    // Initializing window
    let window: pancurses::Window = initscr();
    let max_x: i32 = window.get_max_x();
    let max_y: i32 = window.get_max_y();
    let mut res_x: i32 = 0;
    let mut res_y: i32 = 0;
    let mut delay: Duration = Duration::from_millis(44);
    noecho();
    window.nodelay(true);

    let mut first: bool = true;
    while let Some(Frame) = &mut decoder.read_next_frame().unwrap() {
        // Converting frame to raster image
        if first {
            prev_raster.bytes = Frame.buffer.to_vec();
            delay = Duration::from_millis((Frame.delay * 10) as u64);
            let aspect: f32 = max_x as f32 / (max_y as f32 * 2.0);
            res_y = max_y;
            res_x = (res_y as f32 * aspect) as i32;
            first = false;
        }
        let mut current_image = raster::Image::blank(Frame.width as i32, Frame.height as i32);
        current_image.bytes = Frame.buffer.to_vec();
        let mut complete_blend = raster::editor::blend(
            &prev_raster,
            &current_image,
            raster::BlendMode::Normal,
            1.0,
            raster::PositionMode::TopLeft,
            Frame.left as i32,
            Frame.top as i32,
        ).unwrap();
        prev_raster.bytes = complete_blend.bytes.to_vec();
        raster::filter::grayscale(&mut complete_blend).unwrap();
        raster::transform::resize_exact(&mut complete_blend, res_x * 2, res_y).unwrap();
        string_vector.push(asciify(&complete_blend));
    }
    let mut counter = 0;
    loop {
        for string in &string_vector {
            // Clearing the screen
            window.clear();
            // Printing frame
            window.printw(string);
            // Refreshing the screen
            window.refresh();
            // Waiting for next frame
            sleep(delay);
        }
    }
    Ok(())
}
